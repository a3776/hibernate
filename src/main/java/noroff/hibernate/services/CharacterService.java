package noroff.hibernate.services;

import noroff.hibernate.models.Movie;
import noroff.hibernate.models.MovieCharacter;
import noroff.hibernate.repositories.MovieCharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class CharacterService {

    @Autowired
    private MovieCharacterRepository characterRep;

    public List<MovieCharacter> getAllCharacters(){
        return characterRep.findAll();
    }

    public MovieCharacter getCharacterById(Long id){
        return characterRep.getById(id);
    }

    public MovieCharacter addCharacter(MovieCharacter character) {
        return characterRep.save(character);
    }

    public void deleteCharacter(Long id){
        MovieCharacter character = characterRep.findById(id).get();
        Set<Movie> movies = character.getMovies();
        for(Movie movie : movies) {
            movie.getMovieCharacters().remove(character);
        }
        characterRep.deleteById(id);
    }

    public boolean characterExistsById(Long id){
        return characterRep.existsById(id);
    }
}
