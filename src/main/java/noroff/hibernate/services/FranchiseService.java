package noroff.hibernate.services;

import noroff.hibernate.models.Franchise;
import noroff.hibernate.models.Movie;
import noroff.hibernate.models.MovieCharacter;
import noroff.hibernate.repositories.FranchiseRepository;
import noroff.hibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FranchiseService {

    @Autowired
    private FranchiseRepository franchiseRep;

    @Autowired
    private MovieRepository movieRep;

    public List<Franchise> getAllFranchises(){
        return franchiseRep.findAll();
    }

    public Franchise getFranchiseById(Long id){
        return franchiseRep.getById(id);
    }

    public Franchise addFranchise(Franchise franchise){
        return franchiseRep.save(franchise);
    }

    public boolean franchiseExistsById(Long id){
        return franchiseRep.existsById(id);
    }

    public void deleteFranchise(Long id) {
        Franchise franchise = franchiseRep.getById(id);
        Set<Movie> movies = franchise.getMovies();
        for (Movie movie : movies) {
            movie.setFranchise(null);
        }
        franchiseRep.deleteById(id);
    }

    public Set<Movie> getMoviesFromFranchise(Long id){
        return franchiseRep.getById(id).getMovies();
    }

    public Set<MovieCharacter> getCharactersFromFranchise(Long id){
        Set<MovieCharacter> returnCharacters = new HashSet<>();
        Franchise franchise = franchiseRep.getById(id);
        Set<Movie> movies = franchise.getMovies();
        for (Movie movie : movies){
            for (MovieCharacter mc : movie.getMovieCharacters()){
                returnCharacters.add(mc);
            }
        }
        return returnCharacters;
    }

    public Franchise updateMoviesInFranchise(Long id, String movieIds) {
        movieIds = movieIds.replace("[", "");
        movieIds = movieIds.replace("]", "");
        movieIds = movieIds.replaceAll(" ", "");
        String[] stringArrayIds = movieIds.split(",");
        Long[] movieIdsArray = new Long[stringArrayIds.length];
        for (int i = 0; i < stringArrayIds.length; i++){
            movieIdsArray[i] = Long.valueOf(stringArrayIds[i]);
        }
        Franchise returnFranchise = franchiseRep.findById(id).get();
        Set<Movie> movies = new HashSet<>();
        for (int i = 0; i < movieIdsArray.length; i++){
            if (movieRep.existsById(movieIdsArray[i])){
                movies.add(movieRep.findById(movieIdsArray[i]).get());
            }
        }
        returnFranchise.setMovies(movies);
        return returnFranchise;
    }
}
