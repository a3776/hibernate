package noroff.hibernate.services;

import noroff.hibernate.models.Movie;
import noroff.hibernate.models.MovieCharacter;
import noroff.hibernate.repositories.MovieCharacterRepository;
import noroff.hibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MovieService {
    @Autowired
    private MovieRepository movieRep;

    @Autowired
    private MovieCharacterRepository characterRep;

    public List<Movie> getAllMovies() {
        return movieRep.findAll();
    }

    public Movie getMovieById(Long id) {
        return movieRep.findById(id).get();
    }

    public Movie addMovie(Movie movie) {
        return movieRep.save(movie);
    }

    public Movie updateCharactersInMovie(Long id, String charId){
        charId = charId.replace("[", "");
        charId = charId.replace("]", "");
        charId = charId.replaceAll(" ", "");
        String[] stringArrayIds = charId.split(",");
        Long[] characterIds = new Long[stringArrayIds.length];
        for (int i = 0; i < stringArrayIds.length; i++){
            characterIds[i] = Long.valueOf(stringArrayIds[i]);
        }
        Movie returnMovie = movieRep.findById(id).get();
        Set<MovieCharacter> characters = new HashSet<>();
        for (int i = 0; i < characterIds.length; i++){
            if (characterRep.existsById(characterIds[i])){
                characters.add(characterRep.findById(characterIds[i]).get());
            }
        }
        returnMovie.setMovieCharacters(characters);
        return returnMovie;
    }

    public void deleteMovie(Long id){
        Movie movie = movieRep.getById(id);
        HttpStatus status;
        //Removing the movie from its characters' movies.
        for(MovieCharacter mc : movie.getMovieCharacters()) {
            mc.getMovies().remove(movie);
        }
        //Removing the movie from its franchise's movies.
        movie.getFranchise().getMovies().remove(movie);
        movie.setMovieCharacters(null);
        movieRep.save(movie);
        movieRep.deleteById(id);
    }

    public Set<MovieCharacter> getCharactersFromMovie(Long id){
        return movieRep.getById(id).getMovieCharacters();
    }

    public boolean movieExistsById(Long id) {
        return movieRep.existsById(id);
    }
}
