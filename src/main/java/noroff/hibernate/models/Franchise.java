package noroff.hibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 100)
    private String name;

    private String description;

    @OneToMany(mappedBy = "franchise")
    Set<Movie> movies;

    public Franchise() {}


    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @JsonGetter("movies")
    public Set<Long> movies() {
        if(movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return  movie.getId();
                    }).collect(Collectors.toSet());
        }
        return null;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
