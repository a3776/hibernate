package noroff.hibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 100)
    private String name;

    @Column(length = 100)
    private String alias;


    private String picture;

    @ManyToMany
    @JoinTable(
            name = "character_movie",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    public Set<Movie> movies;

    public MovieCharacter(){}


    public MovieCharacter (String name, String alias, String picture) {
        this.name = name;
        this.alias = alias;
        this.picture = picture;
    }

    @JsonGetter("movies")
    public Set<Long> movies() {
        return movies.stream()
                .map(movie -> {
                    return movie.getId();
                }).collect(Collectors.toSet());
    }


    public void addMovie(Movie movie) {
        this.movies.add(movie);
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Set<Movie> getMovies() {
        return movies;
    }
}
