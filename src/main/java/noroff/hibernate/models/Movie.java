package noroff.hibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 100)
    private String title;

    @Column(length = 100)
    private String genre;

    private int releaseYear;

    @Column(length = 100)
    private String director;

    private String picture;

    private String trailer;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    public Franchise franchise;

    @ManyToMany(mappedBy = "movies")
    public Set<MovieCharacter> movieCharacters;

    public Movie(){}

    public Movie(String title, String genre, int releaseYear, String director, String picture, String trailer) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.picture = picture;
        this.trailer = trailer;
    }

    @JsonGetter("franchise")
    public Long franchise() {
        if(franchise != null){
            return franchise.getId();
        }else{
            return null;
        }
    }

    @JsonGetter("movieCharacters")
    public Set<Long> movieCharactersGetter() {
        if(movieCharacters != null){
            return movieCharacters.stream()
                    .map(character -> {
                        return character.getId();
                    }).collect(Collectors.toSet());
        }
        return null;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public Set<MovieCharacter> getMovieCharacters() {
        return movieCharacters;
    }

    public void setMovieCharacters(Set<MovieCharacter> movieCharacters) {
        this.movieCharacters = movieCharacters;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }
}
