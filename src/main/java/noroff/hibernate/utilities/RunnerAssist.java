package noroff.hibernate.utilities;

import noroff.hibernate.models.Franchise;
import noroff.hibernate.models.Movie;
import noroff.hibernate.models.MovieCharacter;
import noroff.hibernate.repositories.MovieCharacterRepository;
import noroff.hibernate.repositories.FranchiseRepository;
import noroff.hibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class RunnerAssist implements ApplicationRunner {
    @Autowired
    MovieCharacterRepository characterRep;

    @Autowired
    MovieRepository movieRep;

    @Autowired
    FranchiseRepository franchiseRep;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (movieRep.count() == 0 && characterRep.count() == 0 && franchiseRep.count() == 0) {
            MovieCharacter harry = new MovieCharacter("Harry Potter", "The Boy Who Lived", "https://www.imdb.com/title/tt0241527/mediaviewer/rm2113437952/");
            MovieCharacter hermione = new MovieCharacter("Hermoione Granger", "Mudblood", "https://www.imdb.com/title/tt0241527/mediaviewer/rm499213056/?ft0=name&fv0=nm0914612&ft1=image_type&fv1=still_frame");
            MovieCharacter ron = new MovieCharacter("Ron Weasley", "Ron", "https://www.imdb.com/title/tt0241527/mediaviewer/rm1656840960/?ft0=name&fv0=nm0342488&ft1=image_type&fv1=still_frame");
            Movie sorcerer = new Movie("The Sorcerer's Stone", "Adventure, Family, Fantasy", 2001, "Chris Columbus", "https://www.imdb.com/title/tt0241527/mediaviewer/rm683213568/", "https://www.imdb.com/video/vi3115057433?playlistId=tt0241527&ref_=tt_ov_vi");
            Movie chamber = new Movie("The Chamber of Secrets", "Adventure, Family, Fantasy", 2002, "Chris Columbus", "https://www.imdb.com/title/tt0295297/mediaviewer/rm1029675264/", "https://www.imdb.com/video/vi1705771289?playlistId=tt0295297&ref_=tt_ov_vi");
            Movie prisoner = new Movie("The Prisoner of Azkaban", "Adventure, Family, Fantasy", 2004, "Alfonso Cuarón", "https://www.imdb.com/title/tt0304141/mediaviewer/rm3241184256/", "https://www.imdb.com/video/vi2007761177?playlistId=tt0304141&ref_=tt_ov_vi");
            Franchise hp = new Franchise("Harry Potter", "Harry is just a normal boy, until he isn't :0");
            MovieCharacter james = new MovieCharacter("James Bond", "007", "https://www.imdb.com/title/tt1074638/mediaviewer/rm2454302464/");
            MovieCharacter m = new MovieCharacter("M", "M", "https://www.imdb.com/title/tt1074638/mediaviewer/rm2721428480/?ft0=name&fv0=nm0001132&ft1=image_type&fv1=still_frame");
            Movie skyfall = new Movie("Skyfall", "Adventure, Action, Thriller", 2012, "Sam Mendes", "https://www.imdb.com/title/tt1074638/mediaviewer/rm2254713344/", "https://www.imdb.com/video/vi973841433?playlistId=tt1074638&ref_=tt_ov_vi");
            Movie casino = new Movie("Casino Royale", "Adventure, Action, Thriller", 2006, "Martin Campbell", "https://www.imdb.com/title/tt0381061/mediaviewer/rm3667992064/", "https://www.imdb.com/video/vi1859520025?playlistId=tt0381061&ref_=tt_ov_vi");
            Franchise jb = new Franchise("James Bond", "Bond, James Bond ;)");

            franchiseRep.save(hp);
            franchiseRep.save(jb);

            sorcerer.setFranchise(hp);
            chamber.setFranchise(hp);
            prisoner.setFranchise(hp);
            skyfall.setFranchise(jb);
            casino.setFranchise(jb);

            movieRep.save(sorcerer);
            movieRep.save(chamber);
            movieRep.save(prisoner);
            movieRep.save(skyfall);
            movieRep.save(casino);

            Set<Movie> hpMovies = new HashSet<>();

            hpMovies.add(sorcerer);
            hpMovies.add(chamber);
            hpMovies.add(prisoner);

            harry.setMovies(hpMovies);
            hermione.setMovies(hpMovies);
            ron.setMovies(hpMovies);

            Set<Movie> jbMovies = new HashSet<>();

            jbMovies.add(skyfall);
            jbMovies.add(casino);

            james.setMovies(jbMovies);
            m.setMovies(jbMovies);

            characterRep.save(harry);
            characterRep.save(hermione);
            characterRep.save(ron);
            characterRep.save(james);
            characterRep.save(m);
        }

    }
}
