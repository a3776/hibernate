package noroff.hibernate.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.hibernate.models.Movie;
import noroff.hibernate.models.MovieCharacter;
import noroff.hibernate.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @Operation(summary = "Get all Movies from the database")
    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> movies = movieService.getAllMovies();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }

    @Operation(summary = "Get a Movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id){
        Movie returnMovie = new Movie();
        HttpStatus status;
        if(movieService.movieExistsById(id)){
            status = HttpStatus.OK;
            returnMovie = movieService.getMovieById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, status);
    }

    @Operation(summary = "Add a Movie")
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        Movie returnMovie = movieService.addMovie(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie, status);
    }

    @Operation(summary = "Updating movie-characters in selected movie")
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateCharacters(@PathVariable Long id, @RequestBody String charId){
        Movie returnMovie = new Movie();
        HttpStatus status;
        if(!movieService.movieExistsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnMovie, status);
        }
        returnMovie = movieService.updateCharactersInMovie(id, charId);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnMovie, status);
    }

    @Operation(summary = "Delete a Movie by its id")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable Long id){
        HttpStatus status;
        if (!movieService.movieExistsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>("Could not delete movie.", status);
        }
        movieService.deleteMovie(id);
        status = HttpStatus.OK;
        return new ResponseEntity<>("Deleted movie.", status);
    }

    @Operation(summary = "Get all movie-characters in selected movie")
    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<MovieCharacter>> getCharactersFromMovie(@PathVariable Long id){
        Set<MovieCharacter> returnCharacters = new HashSet<>();
        HttpStatus status;
        if (movieService.movieExistsById(id)) {
            status = HttpStatus.OK;
            returnCharacters = movieService.getCharactersFromMovie(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacters, status);
    }
}
