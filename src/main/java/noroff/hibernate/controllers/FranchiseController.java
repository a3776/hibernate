package noroff.hibernate.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.hibernate.models.Franchise;
import noroff.hibernate.models.Movie;
import noroff.hibernate.models.MovieCharacter;
import noroff.hibernate.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseService franchiseService;

    @Operation(summary = "Get all franchises from the database")
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseService.getAllFranchises();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    @Operation(summary = "Get a franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if(franchiseService.franchiseExistsById(id)){
            returnFranchise = franchiseService.getFranchiseById(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    @Operation(summary = "Add a franchise to the database")
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        Franchise returnFranchise = franchiseService.addFranchise(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFranchise, status);
    }

    @Operation(summary = "Delete a franchise from the database dy it's id")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable Long id){
        HttpStatus status;
        if(franchiseService.franchiseExistsById(id)){
            franchiseService.deleteFranchise(id);
            status = HttpStatus.OK;
            return new ResponseEntity<>("Deleted franchise.", status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>("Could not delete franchise.", status);
    }

    @Operation(summary = "Returns all movies from the selected franchise")
    @GetMapping("/{id}/movies")
    public ResponseEntity<Set<Movie>> getMoviesFromFranchise(@PathVariable Long id){
        Set<Movie> returnMovies = new HashSet<>();
        HttpStatus status;
        if (franchiseService.franchiseExistsById(id)) {
            returnMovies = franchiseService.getMoviesFromFranchise(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovies, status);
    }

    @Operation(summary = "Get all movie-characters in selected franchise")
    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<MovieCharacter>> getCharactersFromFranchise(@PathVariable Long id){
        Set<MovieCharacter> returnCharacters = new HashSet<>();
        HttpStatus status;
        if (franchiseService.franchiseExistsById(id)) {
            returnCharacters = franchiseService.getCharactersFromFranchise(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacters, status);
    }

    @Operation(summary = "Updating the movies in the selected franchise")
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateMovies(@PathVariable Long id, @RequestBody String movieIds){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if(!franchiseService.franchiseExistsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            returnFranchise = franchiseService.updateMoviesInFranchise(id, movieIds);
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

}
