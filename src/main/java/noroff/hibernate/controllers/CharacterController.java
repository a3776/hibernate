package noroff.hibernate.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import noroff.hibernate.models.MovieCharacter;
import noroff.hibernate.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/characters")
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    @Operation(summary = "Get all characters from the database")
    @GetMapping()
    public ResponseEntity<List<MovieCharacter>> getAllCharacters(){
        List<MovieCharacter> characters = characterService.getAllCharacters();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }

    @Operation(summary = "Get a character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "character not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getCharacter(@PathVariable Long id){
        MovieCharacter returnCharacter = new MovieCharacter();
        HttpStatus status;
        if(characterService.characterExistsById(id)){
            returnCharacter = characterService.getCharacterById(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacter, status);
    }

    @Operation(summary = "Add a character")
    @PostMapping
    public ResponseEntity<MovieCharacter> addCharacter(@RequestBody MovieCharacter character){
        MovieCharacter returnCharacter = characterService.addCharacter(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCharacter, status);
    }

    @Operation(summary = "Delete a movie-character from the database dy it's id")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable Long id){
        MovieCharacter character = new MovieCharacter();
        HttpStatus status;
        if (characterService.characterExistsById(id)) {
            characterService.deleteCharacter(id);
            status = HttpStatus.OK;
            return new ResponseEntity<>("Deleted character.", status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>("Could not delete character.", status);
        }



    }

}
